<div class="main-sidebar">
        <aside id="sidebar-wrapper">
                <div class="sidebar-brand">
                        <img class="mt-2 mb-5 pb-5" style="width: 95%"
                                src="{{ asset('images/logo.png') }}" alt="">
                </div>
                <div class="sidebar-brand sidebar-brand-sm">
                        <a href="">Mamikos</a>
                </div>
                <ul class="sidebar-menu">
                        @if (auth()->check())
                            
                        @if(auth()->user()->role_id == 1)
                        <li class="menu-header">Dashboard</li>
                        <li class="@if ($title == "Dashboard") active @endif"><a class="nav-link" href="{{ route('owner.dashboard') }}"><i
                                class="fas fa-columns"></i>
                        <span>Dashboard</span></a></li>
                        @endif
                            
                        @endif
                        
                </ul>
                <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="" class="btn btn-primary btn-lg btn-block btn-icon-split">
                                <i class="fas fa-phone"></i> Bantuan
                        </a>
                </div>
        </aside>
</div>