<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocationController extends ApiController
{
    // Location simulation
    protected function index()
    {
        return $this->respondSuccess('Fetched', \App\Models\Location::all());
    }
}
