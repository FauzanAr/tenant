<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{
    protected function store(Request $request)
    {
        try {
            // Validate incoming value
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8|confirmed',
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'role_id' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->respondInvalid($validator->errors());
            }

            // Getting user role is valid or not
            $role = \App\Models\UserRole::find($request['role_id']);

            if (!$role) {
                return $this->respondNotFound('Role not found!');
            }

            // Transaction for secure saving data
            DB::transaction(function () use ($request, $role) {
                \App\Models\User::create([
                    'name' => ucwords($request['name'], " "),
                    'password' => bcrypt($request['password']),
                    'email' => $request['email'],
                    'role_id' => $request['role_id'],
                    'credit_amount' => $role->initial_credit
                ]);
            }, 3);

            return $this->respondSuccess('Account Created');
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function login(Request $request)
    {
        try {
            // Validate incoming value
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|min:8',
            ]);

            if ($validator->fails()) {
                return $this->respondInvalid($validator->errors());
            }

            $user = \App\Models\User::where('email', $request['email'])->first();

            // User Check
            if (!$user || !Hash::check($request['password'], $user->password)) {
                return $this->respondForbidden('Credentials not match');
            }

            return $this->respondSuccess('Login Success', [
                'token' => $user->createToken('token')->plainTextToken
            ]);
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }
}
