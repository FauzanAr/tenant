<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AskLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'kost_id',
    ];

    public function kost()
    {
        return $this->belongsTo(\App\Models\Kost::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
