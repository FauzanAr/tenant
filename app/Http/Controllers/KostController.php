<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class KostController extends ApiController
{
    protected function index(Request $request)
    {
        try {
            // Get only their kost
            return $this->respondSuccess('Fetched', \App\Models\Kost::where('owner_id', $request['user_db']->id)->get());
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function store(Request $request)
    {
        try {
            // Validate incoming value
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'price' => 'required|numeric|min:0',
                'location_id' => 'required',
                'total_rooms' => 'required|integer|min:0'
            ]);

            if ($validator->fails()) {
                return $this->respondInvalid($validator->errors());
            }

            // Location check
            $is_available = $this->is_location_available($request['location_id']);
            if (!$is_available) {
                return $this->respondNotFound('Location not found!');
            }

            // Saving value using transaction
            DB::transaction(function () use ($request) {
                \App\Models\Kost::create([
                    'owner_id' => $request['user_db']->id,
                    'name' => $request['name'],
                    'price' => $request['price'],
                    'location_id' => $request['location_id'],
                    'total_rooms' => $request['total_rooms']
                ]);
            }, 3);

            return $this->respondSuccess('Kost created!');
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'price' => 'required|numeric|min:0',
                'location_id' => 'required',
                'total_rooms' => 'required|integer|min:0'
            ]);

            if ($validator->fails()) {
                return $this->respondInvalid($validator->errors());
            }

            // Kost check
            $kost = $request['kost_db'];

            // Location check
            $is_available = $this->is_location_available($request['location_id']);
            if (!$is_available) {
                return $this->respondNotFound('Location not found!');
            }

            DB::transaction(function () use ($request, $kost) {
                $kost->update([
                    'name' => $request['name'],
                    'price' => $request['price'],
                    'location_id' => $request['location_id'],
                    'total_rooms' => $request['total_rooms']
                ]);
            }, 3);

            return $this->respondSuccess('Kost Updated!', $kost);
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function destroy(Request $request)
    {
        try {
            // Getting kost_db from middleware
            $kost = $request['kost_db'];
            $kost->delete();

            return $this->respondSuccess('Kost Deleted!');
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function ask_room(Request $request)
    {
        try {
            if (is_null($request->query('kost_id'))) {
                return $this->respondNotFound('Kost not found');
            }
            $kost = \App\Models\Kost::find($request->query('kost_id'));
            if (!$kost) {
                return $this->respondNotFound('Kost not found');
            }
            $user = $request->user();
            if ($user->credit_amount < 5) {
                return $this->respondForbidden('Insufficient credit!');
            }
            $active_ask = \App\Models\AskLog::where([
                ['user_id', $user->id],
                ['kost_id', $request->query('kost_id')],
                ['is_responded', false]
            ])->first();

            if ($active_ask) {
                return $this->respondSuccess('Ask success');
            }
            DB::transaction(function () use ($user, $kost) {
                \App\Models\AskLog::create([
                    'user_id' => $user->id,
                    'kost_id' => $kost->id,
                ]);
                $user->credit_amount = $user->credit_amount - 5;
                $user->save();
            });

            return $this->respondSuccess('Ask success');
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function ask_index(Request $request)
    {
        try {
            $user = $request['user_db'];
            // Get kost that owned by the owner
            $kost_ids = \App\Models\Kost::where('owner_id', $user->id)->pluck('id');
            // Display ask only for kost that owned by the owner
            $asks = \App\Models\AskLog::with('kost', 'user')->whereIn('kost_id', $kost_ids)
                ->where('is_responded', 0)
                ->get();
            return $this->respondSuccess('Fetched', $asks);
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function answer_ask(Request $request)
    {
        try {
            // Retrieve from middleware
            $validator = Validator::make($request->all(), [
                'is_available' => 'required|boolean'
            ]);
            if ($validator->fails()) {
                return $this->respondInvalid($validator->errors());
            }
            // Check if front end pass kost_id
            if (!$request['kost_db']) {
                return $this->respondNotFound('Kost not found');
            }
            $ask = \App\Models\AskLog::find($request->query('ask_id'));
            if (!$ask) {
                return $this->respondNotFound('Ask not found');
            }
            // Check if valid ask that answered
            if ($ask->kost_id != $request['kost_db']->id) {
                return $this->respondForbidden('You cant update this record!');
            }
            // Check if ask is answered or not
            if (!$ask->is_responded || is_null($ask->is_available)) {
                DB::transaction(function () use ($request, $ask) {
                    $ask->is_available = $request['is_available'];
                    $ask->is_responded = true;
                    $ask->save();
                });
            }
            return $this->respondSuccess('Ask responded!');
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function show(Request $request)
    {
        try {
            $kost = \App\Models\Kost::with('owner', 'location')->findOrFail($request->query('kost_id'));

            return $this->respondSuccess('Fetched', $kost);
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    protected function user_index(Request $request)
    {
        try {
            $sortyBy = 'price';
            $orderBy = 'ASC';
            $q = null;

            if ($request->has('q')) {
                $q = $request->query('q');
            }
            $kosts = \App\Models\Kost::with('owner', 'location')->search($q)
                ->orderBy($sortyBy, $orderBy)
                ->get();
            return $this->respondSuccess('Fetched', $kosts);
        } catch (\Exception $err) {
            return $this->respondInternalError($err->getMessage());
        }
    }

    private function is_location_available($id)
    {
        $location = \App\Models\Location::find($id);

        return $location ? true : false;
    }
}
