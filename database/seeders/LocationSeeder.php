<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Location::insert([
            [
                'name' => 'Jawa Timur',
                'created_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Jawa Tengah',
                'created_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Jawa Barat',
                'created_at' => \Carbon\Carbon::now(),
            ]
        ]);
    }
}
