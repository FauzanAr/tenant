@extends('layouts.app-dist')
@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Title</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">

                            <h4>View Preview</h4>
                        </div>
                        <div class="card-body">

                            <table id="datatable-1" class="table table-striped table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>KDE</td>
                                    </tr>

                                </tbody>

                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function() {       
        $('#datatable-1').DataTable({ "scrollX": true }).columns.adjust();
    });
</script>
@stop