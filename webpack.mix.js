const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ]);

if (mix.inProduction()) {
    mix.version();
}

mix.styles([
    'resources/dist/bootstrap.min.css',
    'resources/dist/select2/dist/css/select2.min.css',
    'resources/dist/bootstrap-daterangepicker/daterangepicker.css',
    'resources/dist/selectric/public/selectric.css',
    'resources/dist/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
    'resources/dist/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
    'resources/dist/assets/css/style.css',
    'resources/dist/assets/css/components.css',
    'resources/dist/dataTables.bootstrap4.min.css',
    'resources/dist/izitoast/css/iziToast.min.css',
    'resources/dist/prism/prism.css',
    'resources/assets/css/font-awesome.min.css',
], 'public/css/app-dist.css');


mix.scripts([
    'resources/dist/jquery-pwstrength/jquery.pwstrength.min.js',
    'resources/dist/bootstrap-daterangepicker/daterangepicker.js',
    'resources/dist/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
    'resources/dist/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
    'resources/dist/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
    'resources/dist/select2/dist/js/select2.full.min.js',
    'resources/dist/selectric/public/jquery.selectric.min.js',
    'resources/dist/izitoast/js/iziToast.min.js',
    'resources/dist/sweetalert/js/sweetalert.min.js',
    'resources/dist/assets/js/scripts.js',
    'resources/dist/assets/js/custom.js',
    'resources/dist/assets/js/page/forms-advanced-forms.js',
    'resources/dist/assets/js/page/bootstrap-modal.js',
    'resources/dist/prism/prism.js',
    'resources/dist/clipboard.js-master/clipboard.min.js',
    'resources/dist/assets/js/stisla.js',

], 'public/js/app-dist.js');