<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserRoleController extends ApiController
{
    protected function index()
    {
        return $this->respondSuccess('Fetched', \App\Models\UserRole::all());
    }
}
