<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;

class DashboardController extends Controller
{
    protected function index(Request $request)
    {
        $user = Auth::user();
        $title = 'Dasboard';
        if(request()->ajax()){
            $kosts = \App\Models\Kost::where('owner_id', $user->id)
                ->with('location')
                ->orderBy('price', 'ASC')
                ->get();
            return DataTables::of($kosts)
                ->addIndexColumn()
                ->make(true);;
        }
        $locations = \App\Models\Location::all();
        return view('screens.administrator.dashboard', compact('title', 'locations'));
    }
}
