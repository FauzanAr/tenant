<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How to use

1. Open terminal and "git clone https://gitlab.com/FauzanAr/tenant.git"
2. In terminal "composer install"
3. In terminal "npm install"
4. Copy .env.example and rename to .env
5. In terminal "php artisan key:generate"
6. Create database and edit DB_DATABASE name in .env
7. In terminal "php artisan migrate --seed"
8. In terminal "php artisan serve"
9. In terminal "npm run dev"

## Documentation
You can find the documentation in : https://documenter.getpostman.com/view/9140921/TzJsfHmC