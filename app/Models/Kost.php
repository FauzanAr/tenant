<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kost extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id',
        'name',
        'price',
        'location_id',
        'total_rooms'
    ];

    public function scopeSearch($query, $q)
    {
        if ($q == null){
            return $query;
        } 
        return $query
            ->where('name', 'LIKE', "%".$q."%")
            ->orWhere('price', $q)
            ->orWhere('location_id', $q);
    }

    public function owner()
    {
        return $this->belongsTo(\App\Models\User::class, 'owner_id');
    }

    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class);
    }
}
