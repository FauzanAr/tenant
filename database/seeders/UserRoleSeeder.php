<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\UserRole::insert([
            [
                'name' => 'Owner',
                'initial_credit' => null,
                'created_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Regular',
                'initial_credit' => 20,
                'created_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Premium',
                'initial_credit' => 40,
                'created_at' => \Carbon\Carbon::now(),
            ]
        ]);
    }
}
