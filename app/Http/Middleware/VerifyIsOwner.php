<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class VerifyIsOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        $role = \App\Models\UserRole::where('name', 'Owner')->first();
        $r = new ApiController;

        if ($user->role_id !== $role->id) {
            return $r->respondForbidden('You are not owner!');
        }
        // Check if front end passing kost_id identifier, check for the owner
        if (!is_null($request->query('kost_id'))) {
            // Get the kost by user input
            $kost = \App\Models\Kost::find($request->query('kost_id'));
            if(!$kost) {
                return $r->respondNotFound('Kost not found!');
            }
            // Check if user id same as owner id in that kost
            if($user->id != $kost->owner_id){
                return $r->respondForbidden('You are not the owner of the kost');
            }
            $request['kost_db'] = $kost; // Save to request for use in another function
        }
        $request['user_db'] = $user; // Save to request for use in another function
        return $next($request);
    }
}
