<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body>
    <div id="app">
        @include('includes.navbar')
        @include('includes.sidebar')
        <main>
            @yield('content')
        </main>
    </div>  
    <footer class="main-footer">
        <div class="footer-left">
            Copyright &copy; 2021
            
        </div>
        <div class="footer-right">Crafted with ❤ for Mamikos</div>
    </footer>

    @include('includes.script')

</body>
</html>