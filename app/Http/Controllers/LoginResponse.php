<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginResponse extends Controller
{
    public function toResponse($request)
    {
        return redirect()->route('owner.dashboard');
    }
}
