<?php

use App\Http\Controllers\KostController;
use App\Http\Controllers\LocationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserRoleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth
Route::post('/register', [UserController::class, 'store'])->name('user.register');
Route::post('/login', [UserController::class, 'login'])->name('user.login');

// Additional
Route::get('/user-roles', [UserRoleController::class, 'index'])->name('user.roles.index');
Route::get('/locations', [LocationController::class, 'index'])->name('location.index');

// Owner Only
Route::prefix('/owner')->group(function () {
    Route::middleware(['auth:sanctum', 'auth.owner'])->group(function () {
        Route::post('/kost/new', [KostController::class, 'store'])->name('owner.kost.store');
        Route::put('/kost/edit', [KostController::class, 'update'])->name('owner.kost.update');
        Route::delete('kost/delete', [KostController::class, 'destroy'])->name('owner.kost.delete');
        Route::get('/kost', [KostController::class, 'index'])->name('owner.kost.index');
        Route::put('/ask/answer', [KostController::class, 'answer_ask'])->name('owner.ask.answer');
        Route::get('/ask/index', [KostController::class, 'ask_index'])->name('owner.ask.index');
    });
});

// User Only
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/ask', [KostController::class, 'ask_room'])->name('ask_room_availabilty');
    Route::get('/kost/detail', [KostController::class, 'show'])->name('user.show.detail_kost');
    Route::get('/kost/list', [KostController::class, 'user_index'])->name('user.index.kosts');
});
