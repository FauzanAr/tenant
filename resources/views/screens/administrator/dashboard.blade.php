@extends('layouts.app-dist')
@section('content')

<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>{{ $title }}</h1>
    </div>
    <div class="section-body">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>List Kost</h4>
              <div class="card-header-action">
                <button data-toggle="modal" data-target="#exampleModal2" class="btn btn-icon icon-left btn-primary"><i
                    class="fas fa-plus"></i>
                  Tambah Kos
                </button>
              </div>
            </div>
            <div class="card-body">
              <table id="datatable-1" class="table table-striped table-bordered" style="width: 100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Kos</th>
                    <th>Harga</th>
                    <th>Lokasi</th>
                    <th>Jumlah Kamar</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama Kos</th>
                    <th>Harga</th>
                    <th>Lokasi</th>
                    <th>Jumlah Kamar</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal2">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Kost</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('owner.kost.store') }}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name">
                <label for="price">Harga</label>
                <input type="number" class="form-control" name="price">
                <label for="location_id">Lokasi</label>
                <select class="form-control" name="location_id">
                  @foreach ($locations as $location)
                      <option value="{{$location->id}}">{{$location->name}}</option>
                  @endforeach
                </select>
                <label for="total_rooms">Total Kamar</label>
                <input type="number" class="form-control" name="total_rooms">
              </div>
            </div>
          </div>
          <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-icon icon-left"><i class="fas fa-check"></i>
              Confirm</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
      $('#datatable-1').DataTable({
        processing: true,
        serverSide: true,
        scrollX : true,
        ajax: {
            url: '{{ url()->current() }}',
        }, //* Ini gabisa, bikin pusying
        columns: [
            {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    sortable: false,
                    searchable: false
            },
            { 
                data: 'name', 
                name: 'name' 
            },
            { 
                data: 'price', 
                name: 'price' 
            },
            { 
                data: 'location.name', 
                name: 'location.name' 
            },
            { 
                data: 'total_rooms', 
                name: 'total_rooms' 
            },
        ],
        createdRow: function(row, data, dataIndex) {
                $(row).find('td').has('img').addClass("text-center");
                $(row).find('td').addClass("align-middle");
            },
            initComplete: function () {
                this.api().columns().every(function(idx) {
                    var column = this;
                    var title = $(column.header()).text();
                    var input = document.createElement("input");
                    input.style = "padding-right: 12px 8px";
                    input.className = 'form-control form-control-sm';
                    input.placeholder = "Cari " + title + " ...";
                    if(idx != 0 && idx !=7 && idx !=8){
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    }
                });
            }
    });
  });
</script>


@stop